#
# dictionary(my_dictionary ADD some_key some_value)
#
# dictionary(my_dictionary DELETE some_key)
#
# dictionary(my_dictionary GET some_key value_variable)
#
# dictionary(my_dictionary CONTAINS some_key result_variable)
#
# dictionary(my_dictionary COUNT result_variable)
#
# dictionary(my_dictionary ITERATE iterator_variable key_variable value_variable)
#
function(dictionary name command)
	set(keys_property   dictionary_${name}_keys)
	set(value_prefix    dictionary_${name}_value_)

	if(command STREQUAL ADD)

		set(key         ${ARGV2})
		set(value       ${ARGV3})
		message(WARNING "Added ${key}:${value} to dictionary ${name}")
		# Store value at provided key
		set_property(GLOBAL PROPERTY ${value_prefix}${key} ${value})
		# Append key to index
		set_property(GLOBAL APPEND PROPERTY ${keys_property} ${key})

	elseif(command STREQUAL DELETE)

		set(key         ${ARGV2})
		message(WARNING "Removed ${key} from dictionary ${name}")
		set_property(GLOBAL PROPERTY ${value_prefix}${key})
		get_property(keys GLOBAL PROPERTY ${keys_property})
		list(REMOVE_ITEM keys ${key})
		set_property(GLOBAL PROPERTY ${keys_property} ${keys})

	elseif(command STREQUAL GET)

		set(key         ${ARGV2})
		set(variable    ${ARGV3})
		# Get value at provided key and set it to the provided variable in the parent scope
		get_property(value GLOBAL PROPERTY ${value_prefix}${key})
		set(${variable} ${value} PARENT_SCOPE)

	elseif(command STREQUAL CONTAINS)

		set(key         ${ARGV2})
		set(variable    ${ARGV3})
		get_property(is_set GLOBAL PROPERTY ${value_prefix}${key} SET)
		set(${variable} ${is_set} PARENT_SCOPE)

	elseif(command STREQUAL COUNT)

		set(variable         ${ARGV2})
		get_property(keys GLOBAL PROPERTY ${keys_property})
		list(LENGTH keys num_keys)
		set(${variable} ${num_keys} PARENT_SCOPE)

	elseif(command STREQUAL ITERATE)

		set(iterator            ${${ARGV2}})
		set(iterator_variable   ${ARGV2})
		set(key_variable        ${ARGV3})
		set(value_variable      ${ARGV4})

		get_property(keys GLOBAL PROPERTY ${keys_property})
		list(LENGTH keys num_keys)

		if(NOT ${iterator} GREATER 0)
			set(i 0)
		else()
			set(i ${iterator})
		endif()

		list(GET keys ${i} key)
		get_property(value GLOBAL PROPERTY ${value_prefix}${key})

		set(${key_variable} ${key} PARENT_SCOPE)
		set(${value_variable} ${value} PARENT_SCOPE)

		math(EXPR i "${i} + 1")

		if(${i} GREATER ${num_keys} OR ${i} EQUAL ${num_keys})
			set(${iterator_variable} -1 PARENT_SCOPE)
		else()
			set(${iterator_variable} ${i} PARENT_SCOPE)
		endif()

	else()

		message(WARNING "Unknown command '${command}' specified for dictionary() function")

	endif()

endfunction()